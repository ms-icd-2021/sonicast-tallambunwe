# infos sur projet ..

Sonicast est une application web de meteo sonifiée. Tout en offrant une interface agréable aux voyants, elle permet aux non-voyants d'avoir les prévision météologique grace aux sons tel que son nom l'indique. 

# Environnement jenkins ...

Pour obtenir une intégration continue de cette application on utlise l'outil d'intégration continue Jenkins. Cet environnement est déployé dans un conteneurs Docker en local. 

Pour éviter de perdre les informations de connexion à la plateforme, nous connectons les volumes du conteneur à ceux de la machine hote (Ubuntu 20.04 dans notre cas). 

Le port http 80 du conteneur est relier aux port 8080 de la machine hote pour permetre la gestion de la plateforme d'ntégration continue. 

Le lancement du conteneur Jenkins se fait dans le repertoire deploy_jenkins 
contenant le fichier docker-compose tel que suit

$ docker-compose up

# Fichier de configuration ...


## 1.Installation des plugins:
Une fois la plateforme mis en place, nous procédons à l'installation de plugins nécessaire à la création du pipeline. Nous avons notament utilisé les plugins suivant:

    Pour la connexion à Gitlab :

        - Gilab API plugin
        - Gitlab Authentication plugin
        - Gitlab plugin

![Gitlab-plugins](images/captures/Screenshot_2021-05-06_Update_Center__Jenkins__2_.png)

    Pour l'authentification de la connexion à la registry gitlab, nous avons besoin de lier le mot de pass et l'url de la registry sur Gitlab donc nous utilisons les plugins:

        - credentials 
        - credentials Binding 


    Pour l'installation du client docker à fin de faire des commandes docker dans notre conteneur Jenkins:

        - Docker API Plugin
        - Docker Commons Plugin
        - Docker Pipeline
        - Docker plugin
        - Docker Slaves Plugin

![docker_plugins ](images/captures/Screenshot_2021-05-06_Update_Center__Jenkins__3_.png)

    Pour la connection ssh vers le serveur de production:

        - SSH Plugin
        - SSH Agent Plugin
        - SSH Pipeline Steps

![ssh_plugins ](images/captures/Screenshot_2021-05-06_Update_Center__Jenkins__1_.png)

## 2.connexion aux compte gitlab:
Nous utilisons le plugin credentials et credentials Binding pour lier l'url et le token du projet sur Gitlab

![](images/captures/Screenshot_2021-05-06_Configure_System__Jenkins_.png)

## 3.Configuration de la chaine d'intégration continue

La chaine d'intégration continue se fait en créant un pipeline item dans le tableau de bord de Genkins

<< Image creation du pipeline msicd-sonicast-tallambunwe >>

En suite on entre les configurations du pipeline sur L'onglet configurer dans le gauche de l'interface. Ici il s'agit de selectioner le credential de connection à l'API Gitlab puis mettre les configuration de la CI dans le pipeline script et l'appliquer et sauver.


# Lancement ...

Après avoir appliquer on lance le processus dans le l'onglet lancer un build à gauche de l'interface.
Et puis si tout se passe bien vous avez ceci:
![lancement](images/captures/Screenshot_2021-05-06_sonicast__Jenkins__1_.png)

## Resultat

L'application est disponible sur [sonicast-msicd](http://3.86.35.52:8019/) (temporelment)


![mis en production](jenkins_deploy/Result_Sonicast.jpg)

# Amelioration du lancement

Le lancement peut également se faire à l'aide des webhook pour etre déclanché automatiquement après un push sur le repository Gitlab. Pour ce faire il est nécessaire d'avoir notre conteneur Jenkins avec un nom de domaine.

