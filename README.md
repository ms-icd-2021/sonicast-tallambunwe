# Chaine d'Intégration Continue Sonicast-msicd-2021

Ce projet est la réalisation d'une chaine d'intégration continue d'une application microservice web de méteo. Nous utilisons deux outils alternative ( Jenkins et Gitlab-ci ) pour assurer l'intégration continue. 

# Architecture Globale

![architecture globale](documentation/SonicastArchitecture.jpg)

# Technologies utilisées
Nous avons repartie l'application en frontend et backend, chacunne utilisant une technologie voulue par le développeur,dans ce cas nous avons eu du .net pour le backend et php pour le frontend. Pour avoir les données metéo nous avons utilisé l'API [meteomaitics](https://www.meteomatics.com). 

Nous les avons ensuite conteneurisé avec Docker. Les technologies utilisées en pour ce projet en recapitulatif sont:
        
- .net
- php
- Docker
- Docker-compose  
- Gitlab
- Jenkins

# Perspectives

