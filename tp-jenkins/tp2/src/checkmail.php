<?php
require "Email.php";

if (! isset($argv[1])) {
    exit("Usage: php ".basename(__FILE__)." <EMAIL>\n");
}
try {
    \Email::fromString($argv[1]);
    echo $argv[1]." OK\n";
} catch (\Exception $e) {
    echo $argv[1]." KO\n";
}

